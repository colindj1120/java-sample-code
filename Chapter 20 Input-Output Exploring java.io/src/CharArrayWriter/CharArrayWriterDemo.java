package CharArrayWriter;

import java.io.CharArrayWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created CharArrayWriterDemo by Colin for Sample Code on 11/3/2014.
 */

// Demonstrate CharArrayWriter
public class CharArrayWriterDemo
{
    public static void main( String[] args ) throws IOException
    {
        CharArrayWriter writer = new CharArrayWriter();
        String tmp = "This should end up in the array";
        char[] buffer = new char[tmp.length()];

        tmp.getChars(0, tmp.length(), buffer, 0);

        try
        {
            writer.write(buffer);
        }
        catch ( IOException e )
        {
            System.out.println("Error Writing to Buffer. Error: " + e);
        }

        System.out.println("Buffer as a string");
        System.out.println(writer);
        System.out.println("Into Array");

        char[] charArray = writer.toCharArray();
        for ( char aCharArray : charArray )
        {
            System.out.print(aCharArray);
        }

        System.out.println("\nTo a FileWriter()");

        // Use try-with-resources to manage the file stream.
        try ( FileWriter f2 = new FileWriter("test.txt") )
        {
            writer.writeTo(f2);
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }

        System.out.println("Doing a reset");
        writer.reset();

        for ( int i = 0; i < 3; ++i )
        {
            writer.write('X');
        }

        System.out.println(writer);
    }
}
