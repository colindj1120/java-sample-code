package FilenameFilter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created DirListOnly by Colin for Sample Code on 11/2/2014.
 */

// Directory of .HTML files.
public class DirListOnly
{
    public static void main( String[] args )
    {
        String dirname = "/java";
        File f1 = new File(dirname);
        FilenameFilter only = new OnlyExt("html");
        String[] strings = f1.list(only);

        for ( String value : strings )
        {
            System.out.println(value);
        }
    }
}
