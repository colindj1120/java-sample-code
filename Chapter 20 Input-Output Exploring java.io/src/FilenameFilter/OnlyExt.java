package FilenameFilter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created OnlyExt by Colin for Sample Code on 11/2/2014.
 */

public class OnlyExt implements FilenameFilter
{
    private final String ext;

    public OnlyExt(final String ext)
    {
        this.ext = '.' + ext;
    }

    @Override
    public boolean accept( final File dir, final String name )
    {
        return name.endsWith(ext);
    }
}
