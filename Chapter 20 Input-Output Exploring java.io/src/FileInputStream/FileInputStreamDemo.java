package FileInputStream;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created FileInputStreamDemo by Colin for Sample Code on 11/2/2014.
 */

// Demonstrate FileInputStream
// This program uses try-with-resources. It requires JDK 7 or later
public class FileInputStreamDemo
{

    public static final int BYTES_CHUNK = 40;

    public static void main( String[] args )
    {

        // Use try-with-resources to close the stream.
        try ( FileInputStream inputStream = new FileInputStream("FileInputStreamDemo.java") )
        {
            System.out.println("Total Available Bytes: " + inputStream.available());

            int n = inputStream.available() / BYTES_CHUNK;
            for ( int i = 0; i < n; ++i )
            {
                System.out.print(( char ) inputStream.read());
            }

            System.out.println("\nStill Available: " + inputStream.available());

            System.out.println("Reading the next " + n + " with one read(b[])");

            byte[] bytes = new byte[n];
            if ( inputStream.read(bytes) != n )
            {
                System.err.println("couldn't read " + n + " bytes.");
            }

            System.out.println(new String(bytes, 0, n));
            System.out.println("\nStill Available: " + inputStream.available());
            System.out.println("Skipping half of remaining bytes with skip()");
            inputStream.skip(( long ) (inputStream.available() / 2));
            System.out.println("Still Available: " + inputStream.available());

            System.out.println("Reading " + (n / 2) + " into the end of array");
            if ( inputStream.read(bytes, n / 2, n / 2) != (n / 2) )
            {
                System.err.println("couldn't read " + (n / 2) + " bytes.");
            }

            System.out.println(new String(bytes, 0, bytes.length));
            System.out.println("\nStill Available: " + inputStream.available());
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
