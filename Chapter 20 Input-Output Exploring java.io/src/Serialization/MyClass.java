package Serialization;

import java.io.Serializable;

/**
 * Created MyClass by Colin for Java Sample Code on 11/5/2014.
 */
public class MyClass implements Serializable
{
    double d;
    int i;
    String s;

    public MyClass( String s, int i, double d )
    {
        this.s = s;
        this.i = i;
        this.d = d;
    }

    public String toString()
    {
        return String.format("s= %s; i= %d; d= %f", s, i, d);
    }
}
