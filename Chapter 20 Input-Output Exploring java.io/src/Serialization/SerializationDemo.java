package Serialization;

import java.io.*;

/**
 * Created SerializationDemo by Colin for Java Sample Code on 11/5/2014.
 */

// A serialization demo
public class SerializationDemo
{
    public static void main( String[] args )
    {
        // Object serialization

        try ( ObjectOutput objectOutputStream = new ObjectOutputStream(new FileOutputStream("serial")) )
        {
            MyClass object1 = new MyClass("Hello", -7, 2.7e10);
            System.out.println("Object1: " + object1);
            objectOutputStream.writeObject(object1);
        }
        catch ( IOException e )
        {
            System.out.println("Exception during serialization: " + e);
        }

        // Object deserialization

        try ( ObjectInput objectInputStream = new ObjectInputStream(new FileInputStream("serial")) )
        {
            MyClass object2 = ( MyClass ) objectInputStream.readObject();
            System.out.println("Object2: " + object2);
        }
        catch ( ClassNotFoundException | IOException e )
        {
            System.out.println("Exception during deserialzation: " + e);
        }
    }
}
