package DataOutputStream;

import java.io.*;

/**
 * Created DataIODemo by Colin for Java Sample Code on 11/2/2014.
 */

// Demonstrate DataInputStream and DataOutputStream
public class DataIODemo
{
    public static void main( String[] args )
    {
        // First, write the data.
        try ( DataOutputStream dout = new DataOutputStream(new FileOutputStream("Test.dat")) )
        {
            dout.writeDouble(98.6);
            dout.writeInt(1000);
            dout.writeBoolean(true);
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Cannot open file. Error: " + e);
            return;
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }

        // Now, read the data back.
        try ( DataInputStream din = new DataInputStream(new FileInputStream("Test.dat")) )
        {
            double d = din.readDouble();
            int i = din.readInt();
            boolean b = din.readBoolean();

            System.out.printf("here are the values: %.1f %d %b", d, i, b);
        }
        catch ( FileNotFoundException e )
        {
            System.out.println("Cannot Open Input File. Error: " + e);
        }
        catch(IOException e)
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
