package PushbackInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PushbackInputStream;

/**
 * Created PushbackInputStreamDemo by Colin for Java Sample Code on 11/2/2014.
 */

// Demonstrate unread().
public class PushbackInputStreamDemo
{
    public static void main( String[] args )
    {
        String text = "if (a == 4) a = 0;\n";
        byte[] buf = text.getBytes();

        ByteArrayInputStream in = new ByteArrayInputStream(buf);
        int c;
        try ( PushbackInputStream stream = new PushbackInputStream(in) )
        {
            while ( (c = stream.read()) != -1 )
            {
                switch ( c )
                {
                    case '=':
                        if ( (c = stream.read()) == '=' )
                        {
                            System.out.print(".eq.");
                        }
                        else
                        {
                            System.out.print("<-");
                            stream.unread(c);
                        }
                        break;
                    default:
                        System.out.print(( char ) c);
                        break;
                }
            }
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
