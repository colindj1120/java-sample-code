package Directories;

import java.io.File;

/**
 * Created DirList by Colin for Sample Code on 11/1/2014.
 */

// Using directories
public final class DirList
{
    public static void main( final String[] args )
    {
        final String dirname = "/java";
        final File f1 = new File(dirname);

        if ( f1.isDirectory() )
        {
            System.out.println("Directory of " + dirname);
            final String[] list = f1.list();

            for ( final String str : list )
            {
                final File file = new File(dirname + '/' + str);
                if ( file.isDirectory() )
                {
                    System.out.printf("%s is a directory%n", str);
                }
                else
                {
                    System.out.printf("%s is a file%n", str);
                }
            }
        }
        else
        {
            System.out.println(dirname + " is not a directory");
        }
    }
}
