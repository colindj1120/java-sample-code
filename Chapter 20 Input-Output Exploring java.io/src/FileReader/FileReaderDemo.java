package FileReader;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created FileReaderDemo by Colin for Java Sample Code on 11/2/2014.
 */

// Demonstrate FileReader
public class FileReaderDemo
{
    public static void main( String[] args )
    {
        try ( FileReader fr = new FileReader("FileReaderDemo.java") )
        {
            int c;

            // Read and display the file.
            while((c = fr.read()) != -1)
            {
                System.out.print(( char ) c);
            }
        }
        catch(IOException e)
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
