package BufferedInputStream;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created BufferedInputStreamDemo by Colin for Sample Code on 11/2/2014.
 */

// Use buffered input
public class BufferedInputStreamDemo
{
    public static void main( String[] args )
    {
        String text = "This is a &copy; copyright symbol but this is &copy not.\n";
        byte[] buffer = text.getBytes();

        ByteArrayInputStream in = new ByteArrayInputStream(buffer);

        try ( BufferedInputStream inputStream = new BufferedInputStream(in) )
        {
            int c;
            boolean marked = false;
            while ( (c = inputStream.read()) != -1 )
            {
                switch ( c )
                {
                    case '&':
                        if ( marked )
                        {
                            marked = false;
                        }
                        else
                        {
                            inputStream.mark(32);
                            marked = true;
                        }
                        break;
                    case ';':
                        if(marked)
                        {
                            marked = false;
                            System.out.print("(c)");
                        }
                        else
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                    case ' ':
                        if ( marked )
                        {
                            marked = false;
                            inputStream.reset();
                            System.out.print("&");
                        }
                        else
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                    default:
                        if ( !marked )
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                }
            }
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
