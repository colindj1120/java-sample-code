package PushbackReader;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.PushbackReader;

/**
 * Created PushbackReaderDemo by Colin for Sample Code on 11/3/2014.
 */

// Demonstrate unread()
public class PushbackReaderDemo
{
    public static void main( String[] args )
    {
        String tmp = "if (a == 4) a = 0;\n";
        char[] buffer = new char[tmp.length()];
        tmp.getChars(0, tmp.length(), buffer, 0);
        CharArrayReader in = new CharArrayReader(buffer);

        int c;
        try ( PushbackReader reader = new PushbackReader(in) )
        {
            while ( (c = reader.read()) != -1 )
            {
                switch ( c )
                {
                    case '=':
                        if ( (c = reader.read()) == '=' )
                        {
                            System.out.print(".eq.");
                        }
                        else
                        {
                            System.out.print("<-");
                            reader.unread(c);
                        }
                        break;
                    default:
                        System.out.print(( char ) c);
                        break;
                }
            }
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
