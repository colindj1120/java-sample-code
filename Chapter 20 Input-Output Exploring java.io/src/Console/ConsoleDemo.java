package Console;

import java.io.Console;

/**
 * Created ConsoleDemo by Colin for Sample Code on 11/3/2014.
 */

// Demonstrate Console
public class ConsoleDemo
{
    public static void main( String[] args )
    {
        // Obtain a reference to the console.
        Console console = System.console();
        // If no console available, exit.
        if(console == null)
        {
            return;
        }

        // Read a string and then display it.
        String tmp = console.readLine("Enter a string: ");
        console.printf("Here is your string: %s\n", tmp);
    }
}
