package BufferedReader;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.IOException;

/**
 * Created BufferedReaderDemo by Colin for Sample Code on 11/3/2014.
 */

// Use buffered input.
public class BufferedReaderDemo
{
    public static void main( String[] args ) throws IOException
    {
        String tmp = "This is a &copy; copyright symbol but this is &copy not.\n";
        char[] buffer = new char[tmp.length()];
        tmp.getChars(0, tmp.length(), buffer, 0);

        CharArrayReader in = new CharArrayReader(buffer);
        int c;
        boolean marked = false;

        try ( BufferedReader reader = new BufferedReader(in) )
        {
            while ( (c = reader.read()) != -1 )
            {
                switch ( c )
                {
                    case '&':
                        if ( !marked )
                        {
                            reader.mark(32);
                            marked = true;
                        }
                        else
                        {
                            marked = false;
                        }
                        break;
                    case ';':
                        if ( marked )
                        {
                            marked = false;
                            System.out.print("(c)");
                        }
                        else
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                    case ' ':
                        if ( marked )
                        {
                            marked = false;
                            reader.reset();
                            System.out.print("&");
                        }
                        else
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                    default:
                        if ( !marked )
                        {
                            System.out.print(( char ) c);
                        }
                        break;
                }
            }
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
        }
    }
}
