package SequenceInputStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created InputStreamEnumerator by Colin for Java Sample Code on 11/2/2014.
 */
public class InputStreamEnumerator implements Enumeration< FileInputStream >
{
    private Enumeration< String > files;

    public InputStreamEnumerator( Vector< String > files )
    {
        this.files = files.elements();
    }

    @Override
    public boolean hasMoreElements()
    {
        return files.hasMoreElements();
    }

    @Override
    public FileInputStream nextElement()
    {
        try
        {
            return new FileInputStream(files.nextElement());
        }
        catch ( FileNotFoundException e )
        {
            System.out.println("Error: " + e);
            return null;
        }
    }
}
