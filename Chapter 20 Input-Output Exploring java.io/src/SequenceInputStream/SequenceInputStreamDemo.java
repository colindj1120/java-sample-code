package SequenceInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Vector;

/**
 * Created SequenceInputStreamDemo by Colin for Java Sample Code on 11/2/2014.
 */

//Demonstrate sequenced input
public class SequenceInputStreamDemo
{
    public static void main( String[] args )
    {
        int c;
        Vector< String > files = new Vector<>();

        files.addElement("file0.txt");
        files.addElement("file1.txt");
        files.addElement("file2.txt");
        InputStreamEnumerator ise = new InputStreamEnumerator(files);
        InputStream input = new SequenceInputStream(ise);

        try
        {
            while((c = input.read()) != -1)
            {
                System.out.print((char) c);
            }
        }
        catch(NullPointerException e)
        {
            System.out.println("Error in opening the file. Error: " + e);
        }
        catch(IOException e)
        {
            System.out.println("I/O Error: " + e);
        }
        finally
        {
            try
            {
                input.close();
            }
            catch ( IOException e )
            {
                System.out.println("Error Closing SequenceInputStream. Error: " + e);
            }
        }

    }
}
