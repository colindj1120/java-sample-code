package ByteArrayOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created ByteArrayOutputStreamDemo by Colin for Sample Code on 11/2/2014.
 */

// Demonstrate ByteArrayOutputStream.
// This program uses try-with-resources. It requires JDK 7 or later.
public class ByteArrayOutputStreamDemo
{
    public static void main( String[] args )
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String byteArrayString = "This should end up in the array";
        byte[] buf = byteArrayString.getBytes();

        try
        {
            outputStream.write(buf);
        }
        catch ( IOException e )
        {
            System.out.println("Error writing to buffer. Error: " + e);
            return;
        }

        System.out.println("Buffer as a string\n" + outputStream);
        System.out.println("Into array");
        byte[] buf2 = outputStream.toByteArray();
        for ( byte aBuf2 : buf2 )
        {
            System.out.print(( char ) aBuf2);
        }

        System.out.println("\nTo an OutputStream()");

        // Use try-with-resources to manage the file stream.
        try ( FileOutputStream f2 = new FileOutputStream("test.txt") )
        {
            outputStream.writeTo(f2);
        }
        catch ( IOException e )
        {
            System.out.println("I/O Error: " + e);
            return;
        }

        System.out.println("Doing a reset");
        outputStream.reset();

        for ( int i = 0; i < 3; ++i )
        {
            outputStream.write('X');
        }

        System.out.println(outputStream);
    }
}
