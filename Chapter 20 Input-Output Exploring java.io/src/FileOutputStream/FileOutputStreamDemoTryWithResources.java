package FileOutputStream;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created FileOutputStreamDemoTryWithResources by Colin for Sample Code on 11/2/2014.
 */

// Demonstrate FileOutputStream
// This version uses try-with-resources. It requires JDK 7 or later.

public class FileOutputStreamDemoTryWithResources
{
    public static void main( String[] args )
    {
        String source = "Now is the time for all good men\n" +
                        " to come to the aid of their country\n" +
                        " and pay their due taxes.";

        byte[] buf = source.getBytes();

        // Use try-with-resources to close the files.
        try ( FileOutputStream f0 = new FileOutputStream("file4.txt");
              FileOutputStream f1 = new FileOutputStream("file5.txt");
              FileOutputStream f2 = new FileOutputStream("file6.txt") )
        {
            //write to first file
            for ( int i = 0; i < buf.length; i += 2 )
            {
                f0.write(buf[i]);
            }

            //write to the second file
            f1.write(buf);

            //write to the third file
            f2.write(buf, buf.length - (buf.length / 4), buf.length / 4);
        }
        catch ( IOException e )
        {
            System.out.println("An I/O Error Occured. Error: " + e);
        }
    }
}
