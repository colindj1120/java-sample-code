package StackDemo;

/**
 * Created by Colin on 8/25/2014.
 */

// This class defines an integer stack that can hold 10 values
public class Stack
{
    int[] stack = new int[10];
    int tos; // top of stack

    // Initialize top-of-stack
    Stack()
    {
        tos = -1;
    }

    // Push an item onto the stack
    void push( int item )
    {
        if ( tos == 9 )
        {
            System.out.println("Stack is full");
        }
        else
        {
            stack[++tos] = item;
        }
    }

    // Pop an item from the stack
    int pop()
    {
        if(tos < 0)
        {
            System.out.println("Stack underflow.");
            return 0;
        }
        else
        {
            return stack[tos--];
        }
    }
}

