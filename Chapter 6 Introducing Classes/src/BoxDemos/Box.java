package BoxDemos;

/**
 * Created by Colin on 8/14/2014.
 */
public class Box
{
    double width;
    double height;
    double depth;

    // This is the constructor for Box
    Box()
    {
        System.out.println("Constructing Box");
        width = 10;
        height = 10;
        depth = 10;
    }

    // This is the constructor for Box
    Box( double width, double height, double depth )
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    // display volume of a box
    void print_volume()
    {
        System.out.println("Volume is " + (width * height * depth));
    }

    // compute and return volume
    double get_volume()
    {
        return width * height * depth;
    }

    // sets dimensions of box
    void setDimensions(double width, double height, double depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}
