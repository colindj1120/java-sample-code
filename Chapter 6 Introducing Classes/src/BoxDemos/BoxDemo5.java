package BoxDemos;

/**
 * Created by Colin on 8/25/2014.
 */

public class BoxDemo5
{
    public static void main( String[] args )
    {
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        // initialize each box
        mybox1.setDimensions(10, 20, 15);
        mybox2.setDimensions(3, 6, 9);

        // get volume of first box
        System.out.println("Volume is " + mybox1.get_volume());

        // get volume of second box
        System.out.println("Volume is " + mybox2.get_volume());
    }
}
