package BoxDemos;

/**
 * Created by Colin on 8/25/2014.
 */
public class BoxDemo6
{
    public static void main( String[] args )
    {
        // declare, allocate, and initialize Box objects
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        // get volume of first box
        System.out.println("Volume is " + mybox1.get_volume());

        // get volume of second box
        System.out.println("Volume is " + mybox2.get_volume());
    }
}
