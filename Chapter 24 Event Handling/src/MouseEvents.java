import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by Colin on 10/17/2014.
 */

/*
    <applet code="MouseEvents" width=300 height=100>
    </applet>
 */

public class MouseEvents extends Applet implements MouseListener, MouseMotionListener
{
    String msg = "";
    int mouseX, mouseY; // coordinates of mouse

    public void init()
    {
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    // Handle mouse clicked
    @Override public void mouseClicked( MouseEvent e )
    {
        // save coordinates
        mouseX = 0;
        mouseY = 10;
        msg = "Mouse clicked.";
        repaint();
    }

    // Handle button pressed
    @Override public void mousePressed( MouseEvent e )
    {
        //save coordinates
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "Down";
        repaint();
    }

    // Handle button released
    @Override public void mouseReleased( MouseEvent e )
    {
        // save coordinates
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "Up";
        repaint();
    }

    // Handle mouse entered
    @Override public void mouseEntered( MouseEvent e )
    {
        // save coordinates
        mouseX = 0;
        mouseY = 10;
        msg = "Mouse entered.";
        repaint();
    }

    // Handle mouse exited.
    @Override public void mouseExited( MouseEvent e )
    {
        // save coordinates
        mouseX = 0;
        mouseY = 10;
        msg = "Mouse exited.";
        repaint();
    }

    // Handle mouse dragged
    @Override public void mouseDragged( MouseEvent e )
    {
        // save coordinates
        mouseX = e.getX();
        mouseY = e.getY();
        msg = "*";
        showStatus("Dragging mouse at " + mouseX + ", " + mouseY);
        repaint();
    }

    // Handle mouse moved
    @Override public void mouseMoved( MouseEvent e )
    {
        // show status
        showStatus("Moving mouse at " + e.getX() + ", " + e.getY());
    }

    // Display msg in applet window at current X, Y location
    public void paint(Graphics g)
    {
        g.drawString(msg, mouseX, mouseY);

    }
}
