package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */
public class RefDemo
{
    public static void main( String[] args )
    {
        BoxWeight weightBox = new BoxWeight(3, 5, 7, 8.37);
        Box plainbox = new Box();

        System.out.println("Volume of weightBox is " + weightBox.volume());
        System.out.println("Weight of weightBox is " + weightBox.weight);
        System.out.println();

        // assign BoxWeight reference to Box reference
        plainbox = weightBox;

        System.out.println("Volume of plainbox is " + plainbox.volume());

        // The following statement is invalid because plainbox does not define a weight member.
        // System.out.println("Weight of plainbox is " + plainbox.weight);
    }
}
