package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */
public class DemoBoxWeight
{
    public static void main( String[] args )
    {
        BoxWeight mybox1 = new BoxWeight(10, 20, 15, 34.3);
        BoxWeight mybox2 = new BoxWeight(2, 3, 4, 0.076);

        System.out.println("Volume of mybox1 is " + mybox1.volume());
        System.out.println("Weight of mybox1 is " + mybox1.weight);
        System.out.println();

        System.out.println("Volume of mybox2 is " + mybox2.volume());
        System.out.println("Weight of mybox2 is " + mybox2.weight);
    }
}
