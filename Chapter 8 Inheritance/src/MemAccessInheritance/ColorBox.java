package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */

// Here, Box is extended to include color.
public class ColorBox extends Box
{
    int color; // color of box

    ColorBox(double width, double height, double depth, int color)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.color = color;
    }
}
