package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */

// A's j is not accessible here.
public class B extends A
{
    int total;

    public void sum()
    {
        total = i; // + j ERROR, j is not accessible here
    }
}
