package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */

/* In a class hierarchy, private members remain
private to their class.

This program contains an error and will not compile.
 */
public class A
{
    int i; // package public by default
    private int j; // private to A

    void setij(int x, int y)
    {
        i = x;
        j = y;
    }
}
