package MemAccessInheritance;

/**
 * Created by Colin on 9/29/2014.
 */

// Here, Box is extended to include weight
public class BoxWeight extends Box
{
    double weight; // weight of box

    // Construct for BoxWeight
    BoxWeight(double width, double height, double depth, double weight)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.weight = weight;
    }
}
