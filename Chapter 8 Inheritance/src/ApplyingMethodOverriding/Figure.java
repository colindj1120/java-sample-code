package ApplyingMethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */

// Using run-time polymorphism
public class Figure
{
    double dim1, dim2;

    Figure(double a, double b)
    {
        dim1 = a;
        dim2 = b;
    }

    double area()
    {
        System.out.println("Area for Figure is undefined.");
        return 0;
    }
}
