package ApplyingMethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */
public class Rectangle extends Figure
{
    Rectangle( double a, double b )
    {
        super(a, b);
    }

    // override area for Rectangle
    double area()
    {
        System.out.println("Inside Area for Rectangle.");
        return dim1 * dim2;
    }
}
