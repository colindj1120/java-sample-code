package MultiLevelHierarchy;

/**
 * Created by Colin on 10/6/2014.
 */

// Add shipping costs.
public class Shipment extends BoxWeight
{
    double cost;

    // construct clone of an object
    Shipment(Shipment ob)
    {
        super(ob);
        cost = ob.cost;
    }

    // constructor when all parameters are specified
    Shipment(double width, double height, double depth, double weight, double cost)
    {
        super(width, height, depth, weight);
        this.cost = cost;
    }

    // default constructor
    Shipment()
    {
        super();
        cost = -1;
    }

    // constructor used when cube is created
    Shipment(double length, double weight, double cost)
    {
        super(length, weight);
        this.cost = cost;
    }
}
