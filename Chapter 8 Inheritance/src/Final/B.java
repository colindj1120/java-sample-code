package Final;

/**
 * Created by Colin on 10/17/2014.
 */
public class B extends A
{
//    The following is illegal because meth is final in A
//    void meth()
//    {
//        System.out.println("ILLEGAL!");
//    }

//  The follwing is legal because meth2 isn't final in A
    void meth2()
    {
        System.out.println("meth2 in B");
    }
}
