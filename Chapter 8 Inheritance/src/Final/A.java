package Final;

/**
 * Created by Colin on 10/17/2014.
 */
public class A
{
    final void meth()
    {
        System.out.println("This is a final method");
    }

    void meth2()
    {
        System.out.println("This is not a final method");
    }
}
