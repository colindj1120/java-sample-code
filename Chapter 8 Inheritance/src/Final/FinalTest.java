package Final;

/**
 * Created by Colin on 10/17/2014.
 */
public class FinalTest
{
    public static void main( String[] args )
    {
        A a = new A();
        B b = new B();

        a.meth();
        a.meth2();

        System.out.println();

        b.meth();
        b.meth2();
    }
}
