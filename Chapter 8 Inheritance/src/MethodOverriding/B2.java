package MethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */
public class B2 extends A
{
    int k;
    B2( int a, int b, int c )
    {
        super(a, b);
        k = c;
    }

    void show(String msg)
    {
        System.out.println(msg + k);
    }
}
