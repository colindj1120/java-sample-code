package MethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */
public class Override2
{
    public static void main( String[] args )
    {
        B2 subOb = new B2(1,2,3);

        subOb.show("This is k: ");
        subOb.show();
    }
}
