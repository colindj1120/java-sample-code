package MethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */
public class B extends A
{
    int k;
    B( int a, int b, int c)
    {
        super(a, b);
        k = c;
    }

    // diplay k - this overrides show() in A
    void show()
    {
        System.out.println("k: " + k);
    }
}
