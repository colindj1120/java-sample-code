package MethodOverriding;

/**
 * Created by Colin on 10/6/2014.
 */

// Method overriding
public class A
{
    int i, j;
    A(int a, int b)
    {
        i = a;
        j = b;
    }

    // display i and j
    void show()
    {
        System.out.println("i and j: " + i + " " + j);
    }
}
