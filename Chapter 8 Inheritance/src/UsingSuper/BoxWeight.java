package UsingSuper;

/**
 * Created by Colin on 9/29/2014.
 */

// BoxWeight now fully implements all constructors
public class BoxWeight extends Box
{
    double weight; // weight of box

    // construct clone of an object
    BoxWeight(BoxWeight ob) // pass object to constructor
    {
        super(ob);
        weight = ob.weight;
    }

    // constructor when all parameters are specified
    BoxWeight(double width, double height, double depth, double weight)
    {
        super(width, height, depth);
        this.weight = weight;
    }

    // default constructor
    BoxWeight()
    {
        super();
        weight = -1;
    }

    // constructor used when cube is created
    BoxWeight(double len, double weight)
    {
        super(len);
        this.weight = weight;
    }
}
