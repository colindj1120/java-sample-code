package UsingSuper;

/**
 * Created by Colin on 9/29/2014.
 */

// A complete implementation of BoxWeight
public class Box
{
    private double width, height, depth;

    // Construct clone of an object
    Box( Box object ) // pass object to constructor
    {
        width = object.width;
        height = object.height;
        depth = object.depth;
    }

    // Constructor used when all dimensions specified
    Box( double width, double height, double depth )
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    // Constructor used when no dimensions specified
    Box()
    {
        width = -1; // use -1 to indicate
        height = -1; // an uninitialized
        depth = -1; // box
    }

    // Constructor used when cube is created
    Box( double len )
    {
        width = height = depth = len;
    }

    // Compute and return volume
    double volume()
    {
        return width * height * depth;
    }
}
