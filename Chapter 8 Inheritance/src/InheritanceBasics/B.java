package InheritanceBasics;

/**
 * Created by Colin on 9/29/2014.
 */

// Create a subclass by extending class A
public class B extends A
{
    int k;

    public void showk()
    {
        System.out.println("k: " + k);
    }

    public void sum()
    {
        System.out.println("i+j+k: " + (i+j+k));
    }
}
