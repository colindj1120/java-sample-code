package InheritanceBasics;

/**
 * Created by Colin on 9/29/2014.
 */

// A simple example of inheritance

// Create a superclass
public class A
{
    int i, j;

    public void showij()
    {
        System.out.println("i and j: " + i + " " + j);
    }
}
