package DynamicMethodDispatch;

/**
 * Created by Colin on 10/6/2014.
 */
public class C extends A
{
    // override callme()
    void callme()
    {
        System.out.println("Inside C's callme method");
    }
}
