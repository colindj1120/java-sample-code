package DynamicMethodDispatch;

/**
 * Created by Colin on 10/6/2014.
 */
public class Dispatch
{
    public static void main( String[] args )
    {
        A a = new A(); // object of type A
        B b = new B(); // object of type b
        C c = new C(); // object of type c;

        A r; // obtain a reference of type A;

        r = a; // r refers to an A object
        r.callme(); // calls A's version of callme

        r = b; // r refers to a B object
        r.callme(); // calls B's version of callme

        r = c; // r refers to a C object
        r.callme(); // calls C's version of callme
    }
}
