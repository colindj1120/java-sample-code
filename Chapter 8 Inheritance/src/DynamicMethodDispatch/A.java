package DynamicMethodDispatch;

/**
 * Created by Colin on 10/6/2014.
 */

// Dynamic Method Dispatch
public class A
{
    void callme()
    {
        System.out.println("Inside A's callme method");
    }
}
