package DynamicMethodDispatch;

/**
 * Created by Colin on 10/6/2014.
 */
public class B extends A
{
    // override callme()
    void callme()
    {
        System.out.println("Inside B's callme method");
    }
}
