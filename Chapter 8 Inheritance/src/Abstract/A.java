package Abstract;

/**
 * Created by Colin on 10/17/2014.
 */

// A simple demonstration of abstract
public abstract class A
{
    abstract void callme();

    // concrete methods are still allowed in abstract classes
    void callmetoo()
    {
        System.out.println("This is a concrete method.");
    }
}
