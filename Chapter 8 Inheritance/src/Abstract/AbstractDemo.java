package Abstract;

/**
 * Created by Colin on 10/17/2014.
 */
public class AbstractDemo
{
    public static void main( String[] args )
    {
        B b = new B();

        b.callme();
        b.callmetoo();
    }
}
