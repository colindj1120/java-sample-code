package Abstract;

/**
 * Created by Colin on 10/17/2014.
 */
public class B extends A
{
    @Override void callme()
    {
        System.out.println("B's implementation of call me");
    }
}
