package ConstructorExecution;

/**
 * Created by Colin on 10/6/2014.
 */

// Create a subclass by extending class A.
public class B extends A
{
    B()
    {
        System.out.println("Inside B's constructor.");
    }
}
