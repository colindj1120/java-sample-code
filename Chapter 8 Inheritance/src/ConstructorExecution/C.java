package ConstructorExecution;

/**
 * Created by Colin on 10/6/2014.
 */

// Create another subclass by extending B.
public class C extends B
{
    C()
    {
        System.out.println("Inside C's constructor.");
    }
}
