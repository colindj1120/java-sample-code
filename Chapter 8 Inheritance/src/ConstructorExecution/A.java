package ConstructorExecution;

/**
 * Created by Colin on 10/6/2014.
 */

// Demonstrate when constructors are executed

// Create a super class
public class A
{
    A()
    {
        System.out.println("Inside A's constructor.");
    }
}
