/**
 * Created by Colin on 7/31/2014.
 */

// Demonstrate char data type

public class CharDemo
{
    public static void main( String[] args)
    {
        char ch1, ch2;

        ch1 = 88; // code for X
        ch2 = 'Y';

        System.out.println("ch1 and ch2: " + ch1 + " " + ch2);
    }
}
