/**
 * Created by Colin on 7/31/2014.
 */

// Compute distance light travels using long variables

public class Light
{
    public static void main( String[] args)
    {
        int lightspeed;
        long days;
        long seconds;
        long distance;

        // approximate speed of light in miles per second
        lightspeed = 186000;

        days = 1000; // specify number of days here

        seconds = days * 24 * 60 * 60; // convert to seconds

        distance = lightspeed * seconds; // compute distance

        System.out.println("In " + days + " days light will travel about " + distance + " miles.");
    }
}
