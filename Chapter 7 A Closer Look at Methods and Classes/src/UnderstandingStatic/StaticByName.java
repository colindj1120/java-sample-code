package UnderstandingStatic;

/**
 * Created by Colin on 9/25/2014.
 */
public class StaticByName
{
    public static void main( String[] args )
    {
        StaticDemo.callme();
        System.out.println("b = " + StaticDemo.b);
    }
}
