package UnderstandingStatic;

/**
 * Created by Colin on 9/25/2014.
 */
public class StaticDemo
{
    static int a = 42;
    static int b = 99;
    
    static void callme()
    {
        System.out.println("a = " + a);
    }
}
