package AccessControl;

/**
 * Created by Colin on 9/25/2014.
 */

// This program demonstrates the difference between public and private
public class Test
{

    public int b; // public access
    int a; // default access
    private int c; // private access

    // methods to access c

    void setc( int i ) // set c's value
    {
        c = i;
    }

    int getc() // get c's value
    {
        return c;
    }
}

