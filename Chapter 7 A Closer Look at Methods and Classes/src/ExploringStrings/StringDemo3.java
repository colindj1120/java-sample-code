package ExploringStrings;

/**
 * Created by Colin on 9/25/2014.
 */

// Demonstrating String Arrays
public class StringDemo3
{
    public static void main( String[] args )
    {
        String[] str = { "one", "two", "three" };
        for ( int i = 0; i < str.length; ++i )
        {
            System.out.println("str[" + i + "]: " + str[i]);
        }
    }
}
