package OverloadDemo2;

/**
 * Created by Colin on 8/25/2014.
 */
public class OverloadDemo
{
    public static void main( String[] args )
    {
        Overload ob = new Overload();
        int i = 88;

        ob.test();
        ob.test(10, 20);

        ob.test(i); // this will invoke test(double)
        ob.test(123.2); // this will invoke test(double)
    }
}
