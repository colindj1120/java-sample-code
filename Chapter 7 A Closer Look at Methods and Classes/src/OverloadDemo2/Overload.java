package OverloadDemo2;

/**
 * Created by Colin on 8/25/2014.
 */

// Demonstrate method overloading
public class Overload
{
    void test()
    {
        System.out.println("No parameters");
    }

    // Overload test for two integer parameters
    void test(int a, int b)
    {
        System.out.println("a and b: " + a + " " + b);
    }

    // Overload test for a double parameter
    void test(double a)
    {
        System.out.println("Inside test(double) a: " + a);
    }
}
