package Recursion;

/**
 * Created by Colin on 8/26/2014.
 */
public class RecursionDemo
{
    public static void main( String[] args )
    {
        Factorial f = new Factorial();

        System.out.println("Factorial of 3 is " + f.fact(3));
        System.out.println("Factorial of 4 is " + f.fact(4));
        System.out.println("Factorial of 5 is " + f.fact(5));
    }
}
