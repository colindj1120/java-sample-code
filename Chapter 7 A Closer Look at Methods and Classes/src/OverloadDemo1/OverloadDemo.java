package OverloadDemo1;

/**
 * Created by Colin on 8/25/2014.
 */
public class OverloadDemo
{
    public static void main( String[] args )
    {
        Overload ob = new Overload();

        // call all versions of test()
        ob.test();
        ob.test(10);
        ob.test(10, 20);
        System.out.println("Result of ob.test(123.25): " + ob.test(123.25));
    }
}
