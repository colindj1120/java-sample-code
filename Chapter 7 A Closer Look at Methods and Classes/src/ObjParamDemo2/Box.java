package ObjParamDemo2;

/**
 * Created by Colin on 8/14/2014.
 */
public class Box
{
    double width;
    double height;
    double depth;

    // constructor used when no dimensions specified
    Box()
    {
        width = -1; // use -1 to indicate
        height = -1; // an uninitialized
        depth = -1; // box
    }

    // constructor used when all dimensinos specified
    Box( double width, double height, double depth )
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    // constructor used when a cube is created
    Box( double len )
    {
        width = height = depth = len;
    }

    // Notice this constructor. It takes an object of type Box.
    Box(Box ob) // pass object to constructor
    {
        width = ob.width;
        height = ob.height;
        depth = ob.depth;
    }

    // display volume of a box
    void print_volume()
    {
        System.out.println("Volume is " + (width * height * depth));
    }

    // compute and return volume
    double get_volume()
    {
        return width * height * depth;
    }

    // sets dimensions of box
    void setDimensions(double width, double height, double depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}
