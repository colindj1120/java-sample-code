package ObjParamDemo2;

/**
 * Created by Colin on 8/25/2014.
 */
public class OverloadConstructor2
{
    public static void main( String[] args )
    {
        // create boxes using the various constructors
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box();
        Box mycube = new Box(7);

        Box myclone = new Box(mybox1); // create copy of mybox1

        // get volume of first box
        System.out.println("Volume of mybox1 is " + mybox1.get_volume());

        // get volume of second box
        System.out.println("Volume of mybox2 is " + mybox2.get_volume());

        // get volume of cube
        System.out.println("Volume of mycube is " + mycube.get_volume());

        // get volume of clone
        System.out.println("Volume of clone is " + myclone.get_volume());
    }
}
