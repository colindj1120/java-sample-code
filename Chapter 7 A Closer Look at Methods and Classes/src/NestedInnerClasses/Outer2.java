package NestedInnerClasses;

/**
 * Created by Colin on 9/25/2014.
 */

// Define an inner class within a for loop
public class Outer2
{
    int outer_x = 100;

    void test()
    {
        for ( int i = 0; i < 10; ++i )
        {
            class Inner
            {
                void display()
                {
                    System.out.println("display: outer_x = " + outer_x);
                }
            }
            Inner inner = new Inner();
            inner.display();
        }
    }
}
