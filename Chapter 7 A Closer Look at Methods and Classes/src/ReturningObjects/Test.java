package ReturningObjects;

/**
 * Created by Colin on 8/26/2014.
 */

// Returning an object
public class Test
{
    int a;

    Test(int i)
    {
        a = i;
    }

    Test incrByTen()
    {
        Test temp = new Test(a+10);
        return temp;
    }
}
