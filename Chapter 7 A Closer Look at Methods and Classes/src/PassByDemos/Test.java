package PassByDemos;

/**
 * Created by Colin on 8/25/2014.
 */

// Primitive types are passed by value
public class Test
{
    int a, b;

    Test( int i, int j )
    {
        a = i;
        b = j;
    }

    void meth( Test o )
    {
        o.a *= 2;
        o.b /= 2;
    }

    void meth( int i, int j )
    {
        i *= 2;
        j /= 2;
    }
}
