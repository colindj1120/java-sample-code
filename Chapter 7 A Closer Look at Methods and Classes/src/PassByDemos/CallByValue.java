package PassByDemos;

/**
 * Created by Colin on 8/25/2014.
 */
public class CallByValue
{
    public static void main( String[] args )
    {
        Test ob = new Test(15 , 20);

        System.out.println("a and b before call: " + ob.a + " " + ob.b);

        ob.meth(ob.a, ob.b);

        System.out.println("a and b after call: " + ob.a + " " + ob.b);
    }
}
