/**
 * Created by Colin on 8/7/2014.
 */

// Demonstrate the while loop
public class While
{
    public static void main( String[] args )
    {
        int n = 10;

        while (n > 0)
        {
            System.out.println("tick " + n);
            n--;
        }
    }
}
