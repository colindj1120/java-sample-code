/**
 * Created by Colin on 8/7/2014.
 */

// Demonstrate the for loop
public class ForTick
{
    public static void main( String[] args )
    {
        for ( int n = 10; n > 0; --n )
        {
            System.out.println("tick " + n);
        }
    }
}
