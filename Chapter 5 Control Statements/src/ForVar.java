/**
 * Created by Colin on 8/7/2014.
 */

// Parts of the for loop can be empty
public class ForVar
{
    public static void main( String[] args )
    {
        int i = 0;
        boolean done = false;
        for (; !done; ++i )
        {
            System.out.println( "i is " + i );
            if ( i == 10 )
            {
                done = true;
            }
        }
    }
}
