/**
 * Created by Colin on 8/7/2014.
 */

// Using the comma
public class Comma
{
    public static void main( String[] args )
    {
        for ( int a = 1, b = 4; a < b; ++a, --b )
        {
            System.out.println("a = " + a);
            System.out.println("b = " + b);
        }
    }
}
