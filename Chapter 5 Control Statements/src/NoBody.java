/**
 * Created by Colin on 8/7/2014.
 */

// The target of a loop can be empty
public class NoBody
{
    public static void main( String[] args )
    {
        int i = 67, j = 793;

        // find midpoint between i and j
        while(++i < --j);

        System.out.println(" Midpoint is " + i);
    }
}
