/**
 * Created by Colin on 8/8/2014.
 */

// Using break with nested loops
public class BreakLoop3
{
    public static void main( String[] args )
    {
        for ( int i = 0; i < 3; ++i )
        {
            System.out.print("Pass " + i + ": ");
            for ( int j = 0; j < 100; ++j )
            {
                if ( j == 10 )
                {
                    break; // terminate if j is 10
                }

                System.out.print(j + " ");

            }
            System.out.println();
        }
        System.out.println("Loops Complete.");
    }
}
