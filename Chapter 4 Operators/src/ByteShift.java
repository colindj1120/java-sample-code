/**
 * Created by Colin on 8/6/2014.
 */

// Left shifting a byte value.
public class ByteShift
{
    public static void main( String[] args)
    {
        byte a = 64, b;
        int i = a << 2;

        b = (byte) (a<<2);
        System.out.println("Original value of a: " + a);
        System.out.println("i and b: " + i + " " + b);
    }
}
