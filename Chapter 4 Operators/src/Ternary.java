/**
 * Created by Colin on 8/6/2014.
 */

// Demonstrate ?
public class Ternary
{
    public static void main( String[] args)
    {
        int i = 10;
        int k = (i < 0) ? -i : i;
        System.out.println("Absolute value of " + i + " is " + k);

        int i1 = -10;
        k = (i1 < 0) ? -i1 : i1; // get absolute value of i;
        System.out.println("Absolute value of " + i1 + " is " + k);
    }
}
