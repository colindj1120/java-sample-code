/**
 * Created by Colin on 8/6/2014.
 */

// Left shifting as a quick way to multiply by 2
public class MultByTwo
{
    public static void main( String[] args)
    {
        int num = 0xffffffe;

        for(int i = 0; i < 4; i++)
        {
            num <<= 1;
            System.out.println(num);
        }
    }
}
