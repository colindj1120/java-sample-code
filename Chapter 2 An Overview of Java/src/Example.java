/**
 * Created by Colin on 7/31/2014.
 */


/*
    This is a simple Java program
    Call this file "Example.java".
 */
public class Example
{
    // Your program beings with a call to main()
    public static void main( String[] args )
    {
        System.out.println("This is a simple Java program.");
    }
}
