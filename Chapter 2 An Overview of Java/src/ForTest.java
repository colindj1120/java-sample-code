/**
 * Created by Colin on 7/31/2014.
 */

/*
    Demonstate the for loop.

    Call this file "ForTest.java".
 */

public class ForTest
{
    public static void main( String[] args )
    {
        for ( int x = 0; x < 10; x++ )
        {
            System.out.println("This is x: " + x);
        }
    }
}
